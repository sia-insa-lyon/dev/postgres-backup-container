# Container to create PostgreSQL Backups
Minimalisitic container (only 25MB) for backing up Postgres databases.

## Goal

Easily backup your PostgreSQL Database with the swift storage (Object Storage)

Intended to be used with:
1. Kubernetes for creating CronJobs that periodically back up your database.
2. Container Instances that can be scheduled at specified times.
3. Your computer! It's smaller than pgAdmin.

## Helm Values

| Value | Default | Comment |
|:------|---------|---------|
| image.registry | registry.gitlab.com/sia-insa-lyon/postgres-backup-container ||
| image.tag | lastest ||
| schedule | 0 1 * * * | The backup time of your database |
| PGHOST | 'postgres' | The domaine name or ip of your database |
| PGDATABASE | 'postgres' | The name of your database |
| PGUSER | 'postgres' | The user use to backup your database|
| PGPASSWORD | 'password' | The password use with user to backup your database|
| PGPORT | "5432" | The port of your database|
| FILE_NAME | "" | The name of your backup ("%Y-%m-%d_%Hh%M".dump.sql" will be appended at the end of file name)|
| secretEnableDatabase | False | Set "True" if you will use a existing secret to database name|
| nameSecretDatabase | "" | Name of secret for database (used if secretEnableDatabase is at "True") |
| nameKeyDatabase | "" | Name of key for database (used if secretEnableDatabase is at "True") |
| secretEnableUser | False | Set "True" if you will use a existing secret to database user |
| nameSecretUser | "" | Name of secret for user (used if secretEnableUser is at "True") |
| nameKeyUser | "" | Name of key for user (used if secretEnableUser is at "True") |
| secretEnablePassword | False | Set "True" if you will use a existing secret to database password |
| nameSecretPassword | "" | Name of secret for password (used if secretEnablePassword is at "True") |
| nameKeyPassword | "" | Name of key for password (used if secretEnablePassword is at "True") |
| OS_PROJECT_NAME | "" | Name of project|
| OS_AUTH_URL | "" |Url of authentification |
| OS_AUTH_USER | "" | Name of user to use for backup |
| OS_AUTH_KEY | "" | Key of user |
| OS_ENDPOINT | "" | Url of endpoint (Bucket or BlockStorage) |
| OS_BUCKET_NAME | "" | Name of Bucket (BlockStorage) |

## Contributing and Modifying

1. Make your desired changes and build the container:

` docker build -t $DOCKER_USER/postgres-back-up . `

2. Test it locally by executing the command below:

` docker run -v /d/backup:/pg_backup $DOCKER_USER/postgres-back-up `

3. Verify that it is an improvement and commit your changes ;)
