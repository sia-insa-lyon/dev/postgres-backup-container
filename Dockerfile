FROM postgres:11-alpine
RUN apk update \
        && apk upgrade \
        && apk add --no-cache \
        ca-certificates && apk add linux-headers libc-dev python3-dev gcc py3-pip py3-wheel && pip3 install python-swiftclient && pip3 install python-keystoneclient \
        && update-ca-certificates 2>/dev/null

ENV PGHOST='postgres'
ENV PGDATABASE='postgres'
ENV PGUSER='postgres'
ENV PGPASSWORD='password'
ENV OS_ENDPOINT=""
ENV OS_AUTH_TOKEN=""
ENV OS_BUCKET_NAME=""

COPY dumpDatabase.sh .

ENTRYPOINT []
CMD [ "bash", "./dumpDatabase.sh" ]
