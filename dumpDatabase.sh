#!/bin/sh

set -e
set -x

DUMP_FILE_NAME="${FILE_NAME}-$(date +"%Y-%m-%d_%Hh%M").dump.sql"
echo "Creating dump"

pg_dumpall -c -w > dump.sql

if [[ $? -ne 0 ]]; then
  echo "Back up not created, check db connection settings"
  exit 1
fi

swift --os-project-name "${OS_PROJECT_NAME}" -V=3 --os-auth-url "${OS_AUTH_URL}" --os-storage-url "${OS_ENDPOINT}" upload "${OS_BUCKET_NAME}" dump.sql --object-name "${DUMP_FILE_NAME}"

rm dump.sql
